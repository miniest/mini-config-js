import _ from "lodash";

/**
 * Configuration handler class. Holds an object with configuration properties.
 * Data may be set, fetched, and updated within the given configuration.
 */
export class Config {
    /**
     *
     * @param {object} [initData = {}] - data initializing configuration
     */
    constructor(initData = {}) {
        // TODO assert object
        this._data = initData;

        /**
         * Parent configuration object
         */
        this._parentConfig = null;

        /**
         * Path of parent configuration this subconfig is on
         */
        this._parentConfigPath = null;
    }

    /**
     * Set the value on the specified path.
     *
     * @param {string} [path = ""] - path of property
     * @param {any} [value = undefined] - value to store on path
     */
    set(path = "", value = undefined) {
        if (this._parentConfig) {
            return this._parentConfig.set(
                Config._concatenatePaths(this._parentConfigPath, path),
                value
            );
        }

        if (path) {
            _.set(this._data, path, _.cloneDeep(value));
        } else {
            this._data = _.cloneDeep(value);
        }
    }

    /**
     * Gets the value from the specified path.
     *
     * @param {String} path
     * @param {Mixed} defaultVal value returned if the value on the path is not
     * available
     */
    get(path = "", defaultVal = undefined) {
        if (this._parentConfig) {
            return this._parentConfig.get(
                Config._concatenatePaths(this._parentConfigPath, path),
                defaultVal
            );
        }

        return path
            ? _.cloneDeep(_.get(this._data, path, defaultVal))
            : this._data
            ? _.cloneDeep(this._data)
            : defaultVal;
    }

    /**
     * Updates current Config data with a new value. Value is updated on the
     * specified path.
     *
     * @param {String} path destination for the new value
     * @param {Mixed} val value to incorporate into the Config structure
     */
    update(path, val) {
        if (this._parentConfig) {
            return this._parentConfig.update(
                Config._concatenatePaths(this._parentConfigPath, path),
                val
            );
        }

        if (path) {
            let d = this.get(path);
            if (d) {
                _.set(this._data, path, _.merge(d, val));
            } else {
                _.set(this._data, path, val);
            }
        } else {
            _.merge(this._data, val);
        }
    }

    /**
     * Concatenates two paths
     */
    static _concatenatePaths(p1, p2) {
        if (p1) {
            // "" returns false too
            if (p2) {
                return [p1, p2].join(".");
            } else {
                return p1;
            }
        } else {
            return p2;
        }
    }
    /**
     * Create subconfig from path in this configuration
     *
     * @param {String} path destination for the new value
     *
     * @returns {Config} new subconfig
     */
    subconfig(path = "") {
        let subconfig = new Config();

        subconfig._parentConfig = this;
        subconfig._parentConfigPath = path;

        return subconfig;
    }
}
