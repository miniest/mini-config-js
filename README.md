[![coverage report](https://gitlab.com/miniest/mini-config-js/badges/master/coverage.svg)](https://gitlab.com/miniest/mini-config-js/commits/master)
[![pipeline status](https://gitlab.com/miniest/mini-config-js/badges/master/pipeline.svg)](https://gitlab.com/miniest/mini-config-js/commits/master)

# mini-config-js

Configuration management lib for mini people.
