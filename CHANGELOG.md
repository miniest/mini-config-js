<a name="2.0.1"></a>
## [2.0.1](https://gitlab.com/miniest/mini-config-js/compare/v2.0.0...v2.0.1) (2020-04-27)



<a name="2.0.0"></a>
# [2.0.0](https://gitlab.com/miniest/mini-config-js/compare/v1.0.0...v2.0.0) (2020-04-27)


### Bug Fixes

* update non-exiting path with not an object ([d366e34](https://gitlab.com/miniest/mini-config-js/commit/d366e34))


### Features

* add subconfig ([d78c7a4](https://gitlab.com/miniest/mini-config-js/commit/d78c7a4))



# [1.0.0](https://gitlab.com/miniest/mini-config-js/compare/v0.0.5...v1.0.0) (2019-11-26)



## [0.0.5](https://gitlab.com/miniest/mini-config-js/compare/v0.0.4...v0.0.5) (2019-11-26)


### Bug Fixes

* lint issues fixed ([3b5cc15](https://gitlab.com/miniest/mini-config-js/commit/3b5cc15da81d00b0706a09228dda826e72a8035e))



