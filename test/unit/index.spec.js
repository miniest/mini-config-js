/* eslint-env mocha */
import { Config } from "../../index.js";

import chai from "chai";
const assert = chai.assert;

describe("Config", () => {
    it("should instantiate", () => {
        const conf = new Config();

        assert.instanceOf(conf, Config);
    });

    it("should set and get values in Config", () => {
        const conf = new Config();

        // without a path
        conf.set("", {
            test1: true,
            test2: {
                a1: "test",
            },
            testValues: {},
        });
        // with a path
        conf.set("testValues", { a1: true, a2: false });

        assert.isTrue(conf.get("test1"));
        assert.equal(conf.get("test2.a1"), "test");
        assert.isTrue(conf.get("testValues.a1"));
        assert.isFalse(conf.get("testValues.a2"));
        // returns default if can't find a value on the path
        assert.isNull(conf.get("testValues.a4", null));

        conf.set("undefinedValuePath");
        assert.isUndefined(conf.get("undefinedValuePath"));

        conf.set();
        assert.isUndefined(conf.get());
    });

    it("should update values in Config", () => {
        const conf = new Config();

        // before update
        conf.set("", {
            testValues: {
                test1: true,
                test2: {
                    a0: true,
                    a1: false,
                },
            },
        });

        assert.isTrue(conf.get("testValues.test1", null));
        assert.isTrue(conf.get("testValues.test2.a0", null));
        assert.isFalse(conf.get("testValues.test2.a1", true));
        assert.isNull(conf.get("testValues.test2.a2", null));

        // after update
        conf.update("testValues.test2", { a1: true, a2: 3, a3: false });

        assert.isTrue(conf.get("testValues.test1", null));
        assert.isTrue(conf.get("testValues.test2.a0", null));
        assert.isTrue(conf.get("testValues.test2.a1", false));
        assert.equal(conf.get("testValues.test2.a2", null), 3);

        conf.update("", { xxx: 1 });
        assert.equal(conf.get("xxx"), 1);
    });

    it("should concatenate paths correctly", () => {
        assert.equal("aa.bb", Config._concatenatePaths(null, "aa.bb"));
        assert.equal("aa.bb", Config._concatenatePaths(undefined, "aa.bb"));
        assert.equal("aa.bb", Config._concatenatePaths("", "aa.bb"));
        assert.equal("aa.bb", Config._concatenatePaths("aa.bb", null));
        assert.equal("aa.bb", Config._concatenatePaths("aa.bb", undefined));
        assert.equal("aa.bb", Config._concatenatePaths("aa.bb", ""));
        assert.equal("aa.bb.xx.yy", Config._concatenatePaths("aa.bb", "xx.yy"));
    });

    it("should work with subconfig", () => {
        const conf = new Config();

        conf.set("", {
            a: {
                a1: 1,
            },
            b: {
                b1: {
                    c1: 2,
                },
            },
        });

        let sconf = conf.subconfig();
        assert.deepEqual(conf.get(), sconf.get());
        assert.deepEqual(conf.get("a"), sconf.get("a"));

        sconf = conf.subconfig("b.b1");
        assert.deepEqual(conf.get("b.b1"), sconf.get(""));

        sconf.set("c1", 5);
        assert.deepEqual(5, sconf.get("c1"));
        assert.deepEqual(conf.get("b.b1.c1"), sconf.get("c1"));

        sconf.update("", { c2: "xx" });
        assert.deepEqual({ c1: 5, c2: "xx" }, sconf.get(""));
        assert.deepEqual(conf.get("b.b1"), sconf.get(""));

        sconf.update("uuu", "uuu");
        assert.deepEqual("uuu", conf.get("b.b1.uuu"));
    });
});
